from flask import Flask, render_template, request

from settings import SERVER_HOST, SERVER_PORT
from utils.common import scan_wifi_networks, create_wifi_config, set_room_type

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/wifi_setup')
def wifi_setup():
    ap_list = scan_wifi_networks()
    return render_template("wifi_setup.html", ap_list=ap_list)


@app.route('/set_wifi', methods=['POST'])
def set_wifi():
    data = request.form.to_dict()
    create_wifi_config(ssid=data['ssid'], password=data['password'])
    return render_template("index.html")


@app.route('/room_setup')
def room_setup():
    return render_template("room_setup.html")


@app.route('/set_room', methods=['POST'])
def set_room():
    data = request.form.to_dict()
    set_room_type(room_type=data['room_type'], description=data['description'])
    return render_template("index.html")


@app.route('/privacy_policy')
def privacy_policy():
    pp_data = [f"Privacy Policy line #{i}" for i in range(100)]
    return render_template("privacy_policy.html", pp_data=pp_data)


@app.route('/terms_of_use')
def terms_of_use():
    tu_data = [f"Terms of Use line #{i}" for i in range(100)]
    return render_template("terms_of_use.html", tu_data=tu_data)


if __name__ == '__main__':

    app.run(debug=True, host=SERVER_HOST, port=SERVER_PORT)
