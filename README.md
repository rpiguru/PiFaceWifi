# PiFaceWiFi

PiFaceWiFi Project

## Components

- Raspberry Pi Zero W

    https://www.raspberrypi.org/products/raspberry-pi-zero-w/

    https://www.amazon.com/CanaKit-Raspberry-inal%C3%A1mbrico-oficial-alimentaci%C3%B3n/dp/B071L2ZQZX

## Installation

- Flash the Raspbian Buster **Lite** to your micro SD Card and plug it in.

- Configure the WiFi of your RPi Zero W to get connected to the Internet.

- Clone this repository

    ```shell script
      sudo apt update
      sudo apt install -y git
      cd ~
      git clone https://gitlab.com/rpiguru/PiFaceWifi  
    ```
  
- Install everything

    ```shell script
      cd PiFaceWifi
      bash setup.sh
    ```

- And reboot!
