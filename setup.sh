exec &>> ${cur_dir}/install.log

echo "========== Installing PiFace Application =========="

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt update

sudo apt install -y python3-dev python3-pip build-essential cmake

sudo pip3 install -U pip
sudo pip3 install -U setuptools
sudo pip3 install -r ${cur_dir}/requirements.txt

bash ${cur_dir}/setup_wifi.sh

# Enable auto start
sudo apt install -y screen

sudo sed -i -- "s/^exit 0/screen -mS pf -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S pf -X stuff \"cd ${cur_dir////\\/}\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S pf -X stuff \"python3 server.py\\\\r\"\\nexit 0/g" /etc/rc.local
